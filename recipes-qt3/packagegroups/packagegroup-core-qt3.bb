#
# Copyright (C) 2011 Wind River
#

SUMMARY = "Qt version 3"
PR = "r1"
LICENSE = "MIT"

inherit packagegroup

PACKAGES = "${PN}-libs"

RDEPENDS_${PN}-libs = "\
    libqt-mt3 \
"

inherit distro_features_check
REQUIRED_DISTRO_FEATURES = "x11"
